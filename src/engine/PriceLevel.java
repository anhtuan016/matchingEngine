package engine;

import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.omg.CORBA.ORB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class PriceLevel {
    Logger logger = Logger.getLogger(PriceLevel.class.getSimpleName());
    public String pricelevel_link = "https://1-dot-bittrexcln.appspot.com/api/v1/matching/pricelevel";
    Gson gson = new Gson();

    /**
     * PRICELEVEL CONTAINS ALL ORDERS WITH THE SAME PRICE, SAME PAIR AND SAME SIDE.
     */
    private Side side;
    private long price;
    private String pair;
    public ArrayList<Order> ordersList;
    private double totalQuantity;

    public PriceLevel(Side side, long price) {
        this.side = side;
        this.price = price;
        ordersList = new ArrayList<>();
    }

    public PriceLevel(Side side, long price, String pair) {
        this.side = side;
        this.price = price;
        ordersList = new ArrayList<>();
        this.pair = pair;
    }

    public Side getSide() {
        return side;
    }

    public long getPrice() {
        return price;
    }

    public boolean isEmpty() {
        return ordersList.isEmpty();
    }

    public Order add(long orderid, long quantity) {
        Order order = new Order(this, orderid, quantity);
        ordersList.add(order);
        return order;
    }

    public void remove(Order order) {
        System.out.println("GET IN REMOVE");
        ordersList.remove(order);
        logger.info("orderlist size: " + ordersList.size() + " contain " + order.getId() + " is :" + ordersList.contains(order));
        this.setTotalQuantity(this.getTotalQuantity() - order.getQuantity());
    }

    public long match(long orderId, Side side, long quantity, OrderBookListener listener) {

//       ORDER FIRST COME FIRST SERVE.
        while (quantity > 0 && !ordersList.isEmpty()) {
            Order oldestOrder = ordersList.get(0);
            long orderQuantity = oldestOrder.getQuantity();
            if (orderQuantity > quantity) {
                oldestOrder.reduce(quantity);
                listener.match(oldestOrder.getId(), orderId, side, price, quantity, oldestOrder.getQuantity());
                this.setTotalQuantity(this.getTotalQuantity() - quantity);
                quantity = 0;
                logger.info("STARTING TO SEND PRICELEVEL MINUS");
                logger.info("new totalquantity: " + this.getTotalQuantity());
            } else {
                ordersList.remove(0);
                listener.match(oldestOrder.getId(), orderId, side, price, orderQuantity, 0);
                quantity -= orderQuantity;
                logger.severe("New quantity after subtracting: " + quantity);
                this.setTotalQuantity(this.getTotalQuantity() - orderQuantity);
                logger.info("STARTING TO SEND PRICELEVEL MINUS");

            }
        }
        return quantity;
    }

    public double getTotalQuantity() {
//        long i = 0;
//        for (Order order : ordersList) {
//            i += order.getQuantity();
//        }
        return this.totalQuantity;

    }

    public long countTotalQuantity() {
        long i = 0;
        for (Order order : ordersList) {
            i += order.getQuantity();
        }
        return i;

    }

    public void setTotalQuantity(double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }
}
